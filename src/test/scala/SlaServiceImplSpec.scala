import akka.actor.ActorSystem
import org.scalatest._
import org.scalatest.concurrent.TimeLimits._

import scala.concurrent.duration._


class SlaServiceImplSpec extends AsyncFlatSpec with Matchers with RecoverMethods {

  implicit val system: ActorSystem = ActorSystem()
  implicit val db: Db = Db

  val slaService = SlaServiceImpl(250.millis)

  "getSlaByToken" should "process request between 230 and 300 ms" in {
    failAfter(300.millis) {
      val time0 = System.currentTimeMillis()
      slaService.getSlaByToken("user_2_token_1").map(_ => {
        assert(System.currentTimeMillis() - time0 > 230)
      })
    }
  }

  "getSlaByToken" should "return the right Sla for token user_2_token_1" in {
    slaService.getSlaByToken("user_2_token_1").map(sla => {
      val user = db.tokens("user_2_token_1").user
      assert(sla == Sla(user, db.users(user).rps))
    })
  }

  "getSlaByToken" should "return the right Sla for token user_2_token_2" in {
    slaService.getSlaByToken("user_2_token_2").map(sla => {
      val user = db.tokens("user_2_token_2").user
      assert(sla == Sla(user, db.users(user).rps))
    })
  }

  "getSlaByToken" should "throw NoSuchElementException(\"token not found: user_4_token_1\") for token user_4_token_1" in {
    recoverToExceptionIf[NoSuchElementException] {
      slaService.getSlaByToken("user_4_token_1")
    } map (_.getMessage should equal("token not found: user_4_token_1"))
  }

  "getSlaByToken" should "throw NoSuchElementException(\"user not found: user_1234\") for token user_1234_token_1" in {
    recoverToExceptionIf[NoSuchElementException] {
      slaService.getSlaByToken("user_1234_token_1")
    } map (_.getMessage should equal("user not found: user_1234"))
  }

}