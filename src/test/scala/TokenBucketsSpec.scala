import akka.actor.ActorSystem
import org.scalatest._

import scala.concurrent.Future
import scala.concurrent.duration._

class TokenBucketsSpec extends AsyncFlatSpec with Matchers with RecoverMethods {

  case class LoadTestResult(totalRequests: Int, successRequests: Int)

  implicit val system: ActorSystem = ActorSystem()

  loadTestBucketTest(loadRps = 100, testDuration = 3.3.seconds, bucketRps = 10, bucketCount = 1, precision = 0.25.second)
  loadTestBucketTest(loadRps = 1, testDuration = 4.seconds, bucketRps = 1, bucketCount = 1, precision = 1.second)
  loadTestBucketTest(loadRps = 1, testDuration = 4.seconds, bucketRps = 1, bucketCount = 10, precision = 1.second)
  loadTestBucketTest(loadRps = 5, testDuration = 4.seconds, bucketRps = 1, bucketCount = 1, precision = 1.second)
  loadTestBucketTest(loadRps = 15, testDuration = 4.seconds, bucketRps = 10, bucketCount = 100, precision = 1.second)
  loadTestBucketTest(loadRps = 5000, testDuration = 4.seconds, bucketRps = 1, bucketCount = 1, precision = 1.second)
  loadTestBucketTest(loadRps = 5000, testDuration = 4.seconds, bucketRps = 500, bucketCount = 1, precision = 1.second)
  loadTestBucketTest(loadRps = 5000, testDuration = 4.seconds, bucketRps = 5000, bucketCount = 1, precision = 1.second)
  loadTestBucketTest(loadRps = 20000, testDuration = 4.seconds, bucketRps = 10000, bucketCount = 1, precision = 1.second)

  def loadTestBucketTest(loadRps: Int, testDuration: FiniteDuration, bucketRps: Int, bucketCount: Int, precision: FiniteDuration): Unit = {
    if (loadRps < bucketRps) throw new IllegalArgumentException("loadRps should be >= bucketRps")

    val expectedTotalRequests = loadRps * testDuration.toMillis.toInt / 1000
    val minTotalRequests = expectedTotalRequests - loadRps * precision.toMillis.toInt / 1000
    val maxTotalRequests = expectedTotalRequests + loadRps * precision.toMillis.toInt / 1000

    val expectedSuccessRequests = bucketRps * testDuration.toMillis.toInt / 1000
    val minSuccessRequests = expectedSuccessRequests - bucketRps * precision.toMillis.toInt / 1000
    val maxSuccessRequests = expectedSuccessRequests + bucketRps * precision.toMillis.toInt / 1000

    val tokenBuckets: TokenBuckets[String] = TokenBuckets(intervalsPerSecond = 10)
    for (i <- 1 to bucketCount) {
      tokenBuckets.addBucketIfAbsent(s"""key_$i""", requestsPerSecond = bucketRps)
    }

    "loadTestBucket" should
      s"""for each of $bucketCount buckets, return $minTotalRequests <= totalRequests <= $maxTotalRequests
         | and $minSuccessRequests <= successRequests <= $maxSuccessRequests
         | for loadRps = $loadRps, testDuration = $testDuration, rps = $bucketRps and precision = $precision""".stripMargin in
      Future.sequence((1 to bucketCount).map(i => {
        loadTestBucket(tokenBuckets, s"""key_$i""", loadRps, testDuration).map(result => {
          result.totalRequests should (be >= minTotalRequests and be <= maxTotalRequests)
          result.successRequests should (be >= minSuccessRequests and be <= maxSuccessRequests)
        })
      }))
        .map(_.last)
        .map(v => {
          tokenBuckets.destroy()
          v
        })
        .recover({ case t =>
          tokenBuckets.destroy()
          throw t
        })
  }

  /** Generate (loadRps / loadIntervalsPerSecond) every (1 / loadIntervalsPerSecond) seconds during testDuration.
    * (loadRps * testDuration) requests will be generated.
    * (bucket's rps * testDuration) requests should be successful.
    *
    * @return LoadTestResult
    * */
  def loadTestBucket(tokenBuckets: TokenBuckets[String], bucket: String, loadRps: Int, testDuration: FiniteDuration): Future[LoadTestResult] = {
    val loadIntervalsPerSecond = 20

    var totalRequests = 0
    var totalRequestsFloat = 0.0
    var successRequests = 0
    val scheduled = system.scheduler.schedule(Duration.Zero, (1000 / loadIntervalsPerSecond).millis, () => {
      totalRequestsFloat += loadRps.toFloat / loadIntervalsPerSecond
      if (totalRequestsFloat - totalRequests >= 1.0) {
        val requestsToMade = (totalRequestsFloat - totalRequests).toInt
        totalRequests += requestsToMade
        for (i <- 1 to requestsToMade) {
          successRequests += (if (tokenBuckets.tryRemoveTokens(bucket)) 1 else 0)
        }
      }
    })
    Util.delayedFuture(testDuration).map(_ => {
      scheduled.cancel()
      LoadTestResult(totalRequests, successRequests)
    })
  }
}