import akka.actor.ActorSystem
import org.scalatest._

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.Success


class KeyExpirationCacheSpec extends FlatSpec with Matchers {

  implicit val system: ActorSystem = ActorSystem()

  import system.dispatcher

  val ttl = 2000

  val cache: KeyExpirationCache[String] = KeyExpirationCache[String](ttl.millis)

  cache.get("key_1") shouldBe empty
  cache.remove("key_1") shouldBe empty
  cache.put("key_1", "value_1")
  cache.get("key_1") shouldBe Some("value_1")
  cache.put("key_1", "value_2")
  cache.get("key_1") shouldBe Some("value_2")
  cache.remove("key_1") shouldBe Some("value_2")
  cache.remove("key_1") shouldBe empty

  cache.put("key_1", "value_1")
  Thread.sleep(ttl - 200)
  cache.get("key_1") shouldBe Some("value_1")
  Thread.sleep(400)
  cache.get("key_1") shouldBe empty

  // replace a value just before expiration and check that the new value isn't expired prematurely, in the moment when the old value should be expired
  cache.put("key_1", "value_1")
  Thread.sleep(ttl - 200)
  cache.get("key_1") shouldBe Some("value_1")
  cache.put("key_1", "value_2")
  Thread.sleep(400)
  cache.get("key_1") shouldBe Some("value_2")
  cache.remove("key_1")

  cache.getOrElseUpdateAsync("key_1", Util.delayedFuture(500.millis).map(_ => Some("value_1"))) shouldBe empty
  Thread.sleep(400)
  cache.get("key_1") shouldBe empty
  Thread.sleep(200)
  cache.get("key_1") shouldBe Some("value_1")
  cache.remove("key_1")

  // cache.getOrElseUpdateAsync basic check
  cache.getOrElseUpdateAsync("key_1", Util.delayedFuture(500.millis).map(_ => Some("value_1"))) shouldBe empty
  Thread.sleep(400)
  cache.get("key_1") shouldBe empty
  Thread.sleep(200)
  cache.get("key_1") shouldBe Some("value_1")
  cache.remove("key_1")

  // check that cache.getOrElseUpdateAsync doesn't compute value for the key if another computation for the key is already in progress
  var opRecomputedConcurrently = false
  cache.getOrElseUpdateAsync("key_1", Util.delayedFuture(500.millis).map(_ => Some("value_1"))) shouldBe empty
  cache.getOrElseUpdateAsync("key_1", {
    opRecomputedConcurrently = true
    Future.successful(Some("value_2"))
  })
  Thread.sleep(300)
  opRecomputedConcurrently shouldBe false
  cache.get("key_1") shouldBe empty
  cache.getOrElseUpdateAsync("key_1", {
    opRecomputedConcurrently = true
    Future.successful(Some("value_3"))
  })
  Thread.sleep(100)
  cache.get("key_1") shouldBe empty
  Thread.sleep(1000)
  opRecomputedConcurrently shouldBe false
  cache.get("key_1") shouldBe Some("value_1")
}