import akka.actor.ActorSystem
import org.scalatest._

import scala.concurrent.Future
import scala.concurrent.duration._

class ThrottlingServiceImplSpec extends AsyncFlatSpec with Matchers with RecoverMethods {

  case class LoadTestResult(totalRequests: Int, successRequests: Int)

  implicit val system: ActorSystem = ActorSystem()
  implicit val db: Db = Db

  val graceRps = 10
  val slaServiceProcessingDelay: FiniteDuration = 250.millis
  val cachedSlaTtl: FiniteDuration = 2000.millis

  // these tests also test that it allows 10% more requests after 1/10 second
  loadTestBucketTest(loadRps = 100, testDuration = 6.seconds, extraDuration = 0.2.seconds, token = Some("user_1_token_1"), precision = 2.second, extraPrecision = 0.1.seconds)
  loadTestBucketTest(loadRps = 100, testDuration = 4.seconds, extraDuration = 0.2.seconds, token = Some("user_1_token_1"), precision = 1.second, extraPrecision = 0.1.seconds)
  loadTestBucketTest(loadRps = 1000, testDuration = 4.seconds, extraDuration = 0.2.seconds, token = None, precision = 1.second, extraPrecision = 0.1.seconds)
  loadTestBucketTest(loadRps = 1000, testDuration = 4.seconds, extraDuration = 0.2.seconds, token = Some("user_1_token_1"), precision = 1.second, extraPrecision = 0.1.seconds)
  loadTestBucketTest(loadRps = 1000, testDuration = 4.seconds, extraDuration = 0.2.seconds, token = Some("user_2_token_1"), precision = 1.second, extraPrecision = 0.1.seconds)
  loadTestBucketTest(loadRps = 1000, testDuration = 4.seconds, extraDuration = 0.2.seconds, token = Some("user_2_token_2"), precision = 1.second, extraPrecision = 0.1.seconds)
  loadTestBucketTest(loadRps = 1000, testDuration = 4.seconds, extraDuration = 0.2.seconds, token = Some("user_3_token_1"), precision = 1.second, extraPrecision = 0.1.seconds)
  loadTestBucketTest(loadRps = 5000, testDuration = 4.seconds, extraDuration = 0.2.seconds, token = Some("user_3_token_2"), precision = 1.second, extraPrecision = 0.1.seconds)
  loadTestBucketTest(loadRps = 5000, testDuration = 4.seconds, extraDuration = 0.2.seconds, token = Some("user_3_token_3"), precision = 1.second, extraPrecision = 0.1.seconds)
  loadTestBucketTest(loadRps = 5000, testDuration = 4.seconds, extraDuration = 0.2.seconds, token = Some("user_5_token_1"), precision = 1.second, extraPrecision = 0.1.seconds)
  loadTestBucketTest(loadRps = 20000, testDuration = 4.seconds, extraDuration = 0.2.seconds, token = Some("user_1234_token_1"), precision = 1.second, extraPrecision = 0.1.seconds)

  def loadTestBucketTest(loadRps: Int, testDuration: FiniteDuration, extraDuration: FiniteDuration, token: Option[String], precision: FiniteDuration, extraPrecision: FiniteDuration): Unit = {
    val bucketRps = token.flatMap(db.tokens.get).map(_.user).flatMap(db.users.get).map(_.rps).getOrElse(graceRps)
    if (loadRps < bucketRps) throw new IllegalArgumentException("loadRps should be >= bucketRps")

    val expectedTotalRequests = loadRps * testDuration.toMillis.toInt / 1000
    val minTotalRequests = expectedTotalRequests - loadRps * precision.toMillis.toInt / 1000
    val maxTotalRequests = expectedTotalRequests + loadRps * precision.toMillis.toInt / 1000

    val expectedSuccessRequests = bucketRps * testDuration.toMillis.toInt / 1000
    val minSuccessRequests = expectedSuccessRequests - bucketRps * precision.toMillis.toInt / 1000
    val maxSuccessRequests = expectedSuccessRequests + bucketRps * precision.toMillis.toInt / 1000

    val expectedExtraSuccessRequests = bucketRps * extraDuration.toMillis.toInt / 1000
    val minExtraSuccessRequests = expectedExtraSuccessRequests - bucketRps * extraPrecision.toMillis.toInt / 1000
    val maxExtraSuccessRequests = expectedExtraSuccessRequests + bucketRps * extraPrecision.toMillis.toInt / 1000

    val throttlingService = ThrottlingServiceImpl(graceRps, SlaServiceImpl(slaServiceProcessingDelay), tokenBucketIntervalsPerSecond = 10, cachedSlaTtl)

    "loadTestBucket" should
      s"""for token $token return $minTotalRequests <= totalRequests <= $maxTotalRequests
         | and $minSuccessRequests <= successRequests <= $maxSuccessRequests
         | and $minExtraSuccessRequests <= extraSuccessRequests <= $maxExtraSuccessRequests
         | for loadRps = $loadRps, testDuration = $testDuration, rps = $bucketRps and precision = $precision""".stripMargin in
      loadTestBucket(throttlingService, token, loadRps, testDuration, extraDuration).map({ case Seq(result1, result2) =>
        result1.totalRequests should (be >= minTotalRequests and be <= maxTotalRequests)
        result1.successRequests should (be >= minSuccessRequests and be <= maxSuccessRequests)
        (result2.successRequests - result1.successRequests) should (be >= minExtraSuccessRequests and be <= maxExtraSuccessRequests)
      })
        .map(v => {
          throttlingService.destroy()
          v
        })
        .recover({ case t =>
          throttlingService.destroy()
          throw t
        })
  }

  /** Generate (loadRps / loadIntervalsPerSecond) every (1 / loadIntervalsPerSecond) seconds during testDuration.
    * (loadRps * testDuration) requests will be generated.
    * (bucket's rps * testDuration) requests should be successful.
    *
    * @return LoadTestResult after testDuration and after extraDuration
    **/
  def loadTestBucket(throttlingService: ThrottlingService, token: Option[String], loadRps: Int, testDuration: FiniteDuration, extraDuration: FiniteDuration): Future[Seq[LoadTestResult]] = {
    val loadIntervalsPerSecond = 20

    var totalRequests = 0
    var totalRequestsFloat = 0.0
    var successRequests = 0
    val scheduled = system.scheduler.schedule(Duration.Zero, (1000 / loadIntervalsPerSecond).millis, () => {
      totalRequestsFloat += loadRps.toFloat / loadIntervalsPerSecond
      if (totalRequestsFloat - totalRequests >= 1.0) {
        val requestsToMade = (totalRequestsFloat - totalRequests).toInt
        totalRequests += requestsToMade
        for (i <- 1 to requestsToMade) {
          successRequests += (if (throttlingService.isRequestAllowed(token)) 1 else 0)
        }
      }
    })
    val future1 = Util.delayedFuture(testDuration).map(_ => {
      LoadTestResult(totalRequests, successRequests)
    })
    val future2 = Util.delayedFuture(testDuration + extraDuration).map(_ => {
      scheduled.cancel()
      LoadTestResult(totalRequests, successRequests)
    })
    Future.sequence(Seq(future1, future2))
  }
}