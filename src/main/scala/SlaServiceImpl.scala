import java.util.NoSuchElementException

import akka.actor.ActorSystem

import scala.concurrent.Future
import scala.concurrent.duration._

class SlaServiceImpl(val processingDelay: FiniteDuration)(implicit system: ActorSystem, db: Db) extends SlaService {

  import system.dispatcher

  /** @throws NoSuchElementException () */
  def getSlaByToken(token: String): Future[Sla] = {
    Util.delayedFuture(processingDelay).map(_ => {
      (for {
        user <- db.tokens.get(token).toRight(new NoSuchElementException("token not found: " + token)).map(_.user)
        rps <- db.users.get(user).toRight(new NoSuchElementException("user not found: " + user)).map(_.rps)
      } yield Sla(user, rps)).toTry.get
    })
  }
}

object SlaServiceImpl {
  def apply(processingDelay: FiniteDuration)(implicit system: ActorSystem, db: Db) = new SlaServiceImpl(processingDelay)
}