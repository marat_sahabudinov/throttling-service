/**
  * Contains sample tables. Used for testing
  */
trait Db {

  case class User(rps: Int)

  case class Token(user: String)

  val users: Map[String, User]
  val tokens: Map[String, Token]
}

object Db extends Db {
  val users = Map(
    "user_0" -> User(2),
    "user_1" -> User(20),
    "user_2" -> User(50),
    "user_3" -> User(100),
    "user_4" -> User(200),
    "user_5" -> User(500)
    // no user_1234
  )

  val tokens = Map(
    "user_0_token_1" -> Token("user_0"),

    "user_1_token_1" -> Token("user_1"),

    "user_2_token_1" -> Token("user_2"),
    "user_2_token_2" -> Token("user_2"),

    "user_3_token_1" -> Token("user_3"),
    "user_3_token_2" -> Token("user_3"),
    "user_3_token_3" -> Token("user_3"),

    // no tokens for user_4
    // "user_4_token_1" -> Token("user_4"),

    "user_5_token_1" -> Token("user_5"),

    // no user_1234
    "user_1234_token_1" -> Token("user_1234")
  )
}