import java.util.NoSuchElementException

import scala.concurrent.Future

trait SlaService {
  /** @throws NoSuchElementException () */
  def getSlaByToken(token: String): Future[Sla]
}