import java.util.concurrent.atomic.AtomicInteger

import akka.actor.{ActorSystem, Cancellable}

import scala.collection._
import scala.concurrent.duration._

/** Used for a rate limiting on a per-key basis.
  * It has a timer that adds (requestsPerSecond * tokensPerRequest / intervalsPerSecond) tokens
  * every (1000 / intervalsPerSecond) milliseconds, but checks that the bucket size is not greater than (requestsPerSecond * tokensPerRequest).
  * On each {tryRemoveTokens} call, (tokensPerRequest) tokens is removed from the bucket.
  * Multiplier (tokensPerRequest) is introduced to support small (requestsPerSecond) values and keep using integers instead of floating-point numbers
  * */
class TokenBuckets[T](intervalsPerSecond: Int)(implicit system: ActorSystem) {

  if (intervalsPerSecond < 1 || intervalsPerSecond > 1000)
    throw new IllegalArgumentException("intervalsPerSecond should be 1 <= n <= 1000")


  import TokenBuckets._
  import system.dispatcher

  private val tokensPerRequest: Int = 1000
  private val buckets: concurrent.Map[T, Bucket] = concurrent.TrieMap()


  private val scheduled: Cancellable = system.scheduler.schedule((1000 / intervalsPerSecond).millis, (1000 / intervalsPerSecond).millis, () => {
    buckets.values.foreach(bucket => {
      tryModifyAndSet(bucket.tokens, v0 => {
        if (v0 < bucket.requestsPerSecond * tokensPerRequest)
          Some(Math.min(bucket.requestsPerSecond * tokensPerRequest, v0 + bucket.requestsPerSecond * tokensPerRequest / intervalsPerSecond))
        else None
      })
    })
  })


  /** Creates a bucket of given `requestsPerSecond` capacity if it doesn't exist */
  def addBucketIfAbsent(key: T, requestsPerSecond: Int): Unit = buckets.putIfAbsent(key, Bucket(requestsPerSecond, new AtomicInteger()))

  /** Removes a bucket */
  def removeBucket(key: T): Unit = buckets.remove(key)

  /** Returns RPS */
  def getRps(key: T): Option[Int] = buckets.get(key).map(_.requestsPerSecond)

  /** Removes tokens from the bucket if there are enough tokens and returns {true}.
    * Otherwise, returns {false}
    *
    * @return {true} if tokens were removed, {false} otherwise
    */
  def tryRemoveTokens(key: T): Boolean = {
    buckets.get(key) match {
      case Some(bucket) =>
        tryModifyAndSet(bucket.tokens, v0 => {
          val v = v0 - tokensPerRequest
          if (v >= 0) Some(v) else None
        })
      case _ => false
    }
  }

  /** Stops the bucket-refill timer */
  def destroy(): Unit = scheduled.cancel()

}

object TokenBuckets {
  def apply[T](intervalsPerSecond: Int)(implicit system: ActorSystem) = new TokenBuckets[T](intervalsPerSecond)

  private case class Bucket(requestsPerSecond: Int, tokens: AtomicInteger)

  /** Atomically modifies the `number` applying `op` to it.
    *
    * @param number the number to modify
    * @param op     the function that is applied to the number. It can be called multiple times.
    *               If result is defined, the resulting value will be assigned to the number and {true} will be returned.
    *               Otherwise, {false} will be returned.
    * @return true if `op` result is defined
    */
  private def tryModifyAndSet(number: AtomicInteger, op: Int => Option[Int]): Boolean = {
    var v0 = 0
    var v: Option[Int] = None
    do {
      v0 = number.get()
      v = op(v0)
    } while (v.isDefined && !number.compareAndSet(v0, v.get))
    v.isDefined
  }
}