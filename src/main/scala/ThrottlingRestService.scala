import akka.actor.ActorSystem
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer


trait ThrottlingRestService {
  implicit val system: ActorSystem
  implicit val materializer: ActorMaterializer
  val throttlingService: ThrottlingService

  val route: Route =
    get {
      parameters('token.?) { token =>
        if (throttlingService.isRequestAllowed(token)) {
          complete(StatusCodes.OK)
        } else {
          complete(StatusCodes.TooManyRequests)
        }
      }
    }
}
