import akka.actor.{ActorSystem, Cancellable}

import scala.collection._
import scala.concurrent.Future
import scala.concurrent.duration.FiniteDuration

/** A cache with timeout expiration for keys.
  * Doesn't compute a value for the key if another computation for this key is already in progress.
  */
class KeyExpirationCache[T](keyTimeToLive: FiniteDuration)(implicit system: ActorSystem) {

  import system.dispatcher

  // map holds key and value, and a Cancellable to cancel delayed key removal in case the value is updated
  private val map: concurrent.Map[String, (T, Cancellable)] = concurrent.TrieMap()

  private val inProgressComputations: concurrent.Map[String, Unit] = concurrent.TrieMap()

  def get(key: String): Option[T] = map.get(key).map(_._1)

  /** Same as get(key: String), but starts asynchronous value computation in the background if the value is not present.
    * Once computed, assigns the computed value for the key.
    * Doesn't compute a value for the key if another computation for this key is already in progress
    *
    * @param op returned future contains Some if the computation was successful and None otherwise */
  def getOrElseUpdateAsync(key: String, op: => Future[Option[T]]): Option[T] = {
    // TODO recompute value if it's still present but expires soon
    val cachedValue = get(key)
    if (cachedValue.isEmpty && !inProgressComputations.contains(key)) {
      inProgressComputations.put(key, ())
      op.map(valueOpt => {
        inProgressComputations.remove(key)
        valueOpt.foreach(value => put(key, value))
      })
    }
    cachedValue
  }

  def put(key: String, value: T): Unit = {
    // Using scheduler instead of new Thread() + Thread.sleep() or Timer to avoid creating many threads
    val cancellable = system.scheduler.scheduleOnce(keyTimeToLive, () => remove(key))
    map.put(key, (value, cancellable)) match {
      case Some((_, c)) => c.cancel()
      case None =>
    }
  }

  def remove(key: String): Option[T] = {
    map.remove(key) match {
      case Some((value, cancellable)) => cancellable.cancel()
        Some(value)
      case None => None
    }
  }

  def clear(): Unit = {
    inProgressComputations.clear()
    map.clear()
  }
}

object KeyExpirationCache {
  def apply[T](keyTimeToLive: FiniteDuration)(implicit system: ActorSystem) = new KeyExpirationCache[T](keyTimeToLive)
}