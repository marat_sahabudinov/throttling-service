import akka.actor.ActorSystem

import scala.concurrent.duration._

class ThrottlingServiceImpl(val graceRps: Int, val slaService: SlaService,
                            tokenBucketIntervalsPerSecond: Int, cachedSlaTimeToLive: FiniteDuration)
                           (implicit db: Db, system: ActorSystem) extends ThrottlingService {

  if (graceRps < 0) throw new IllegalArgumentException("graceRps should be >= 0")
  if (tokenBucketIntervalsPerSecond < 1 || tokenBucketIntervalsPerSecond > 1000)
    throw new IllegalArgumentException("tokenBucketIntervalsPerSecond should be 1 <= n <= 1000")

  import ThrottlingServiceImpl._
  import system.dispatcher

  private val tokenToSlaCache: KeyExpirationCache[Sla] = KeyExpirationCache(cachedSlaTimeToLive)

  // TODO use tokenBuckets.removeBucket() and .destroy()
  // key is Some(user) or None for unauthorized user
  private val tokenBuckets: TokenBuckets[Option[String]] = TokenBuckets(tokenBucketIntervalsPerSecond)

  def isRequestAllowed(token: Option[String]): Boolean = {
    val sla: UserWithRps = token.flatMap(t => {
      tokenToSlaCache.getOrElseUpdateAsync(t, {
        slaService.getSlaByToken(t).map(sla => Some(sla)).recover { case _ => None }
      })
    })
      .map(sla => UserWithRps(Some(sla.user), sla.rps))
      .getOrElse(UserWithRps(None, graceRps))

    tokenBuckets.addBucketIfAbsent(sla.user, sla.rps)
    tokenBuckets.tryRemoveTokens(sla.user)
  }

  def destroy(): Unit = {
    tokenToSlaCache.clear()
    tokenBuckets.destroy()
  }
}

object ThrottlingServiceImpl {
  def apply(graceRps: Int, slaService: SlaService,
            tokenBucketIntervalsPerSecond: Int, cachedSlaTimeToLive: FiniteDuration)
           (implicit db: Db, system: ActorSystem) =
    new ThrottlingServiceImpl(graceRps, slaService, tokenBucketIntervalsPerSecond, cachedSlaTimeToLive)

  // unauthorized users have user = None
  case class UserWithRps(user: Option[String], rps: Int)

}