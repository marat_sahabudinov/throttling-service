trait ThrottlingService {
  protected val graceRps: Int
  protected val slaService: SlaService

  // Should return true if the request is within allowed RPS.
  def isRequestAllowed(token: Option[String]): Boolean
}