import akka.actor.ActorSystem

import scala.concurrent.duration.FiniteDuration
import scala.concurrent.{Future, Promise}
import scala.util.{Success, Try}

object Util {
  /** Executes `op` synchronously on the ActorSystem thread pool using the system's scheduler
    * instead of new Thread() + Thread.sleep() or Timer to avoid creating many threads
    */
  def delayedFuture(delay: FiniteDuration)(implicit system: ActorSystem): Future[Unit] = {
    import system.dispatcher
    val promise = Promise[Unit]
    system.scheduler.scheduleOnce(delay, () => promise.complete(Success()))
    promise.future
  }
}
