import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.ServerBinding
import akka.stream.ActorMaterializer

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.io.StdIn

class Main(implicit val system: ActorSystem, implicit val materializer: ActorMaterializer, implicit val throttlingService: ThrottlingService) extends ThrottlingRestService {
  def startServer(host: String, port: Int): Future[ServerBinding] = {
    Http().bindAndHandle(route, host, port)
  }
}

object Main {
  def main(args: Array[String]): Unit = {
    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: ActorMaterializer = ActorMaterializer()
    implicit val executionContext: ExecutionContextExecutor = system.dispatcher
    // Database used by service for storing user and token info
    implicit val db = Db


    /**
      * Test it by querying http://localhost:9345/?token=user_0_token_1
      * it should allow 2 requests per second
      */
    val port = 9345
    val graceRps = 10
    val slaServiceProcessingDelay: FiniteDuration = 250.millis
    val cachedSlaTtl: FiniteDuration = 2000.millis
    implicit val throttlingService: ThrottlingService = ThrottlingServiceImpl(graceRps, SlaServiceImpl(slaServiceProcessingDelay), tokenBucketIntervalsPerSecond = 10, cachedSlaTtl)

    val bindingFuture = new Main().startServer("localhost", port)
    println(s"Server online at http://localhost:$port/\nPress RETURN to stop...")
    StdIn.readLine()
    bindingFuture.flatMap(_.unbind())
      .onComplete(_ => system.terminate())
  }

}